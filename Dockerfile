FROM node:10.15.1 AS BUILD

# Environments
ENV CODER_VERSION=1.1156-vsc1.33.1
# base repository https://github.com/cdr/code-server.git
RUN apt-get update && apt-get install -y \
    libxkbfile-dev \
    libsecret-1-dev \
 && git clone https://github.com/cdr/code-server.git -b ${CODER_VERSION} /src/
# Ensure latest yarn.
RUN npm install -g yarn@1.13

WORKDIR /src/

RUN yarn && NODE_ENV=production yarn task build:server:binary

FROM alpine:3.10.0

RUN apk --no-cache add alpine-sdk \
    openssl \
    net-tools \
    dumb-init \
    vim \
    wget \
    shadow

RUN adduser --gecos '' --disabled-password coder \
 && echo "coder ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/nopasswd

USER coder
RUN mkdir -p /home/coder/project

WORKDIR /home/coder/project

VOLUME [ "/home/coder/project" ]

COPY --from=BUILD /src/packages/server/cli-linux-x64 /usr/local/bin/code-server
EXPOSE 8443

ENTRYPOINT ["dumb-init", "code-server"]